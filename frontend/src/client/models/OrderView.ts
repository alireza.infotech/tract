/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type OrderView = {
    quantity: number;
    status?: (string | null);
    farm_id: (number | null);
    product_id: (number);
    date: string;
    id: number;
};

