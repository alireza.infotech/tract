
import { Link } from "@tanstack/react-router"
import type React from "react"

const NotFound: React.FC = () => {
  return (
    <Link to="/">
      Go back
    </Link>
  )
}

export default NotFound
